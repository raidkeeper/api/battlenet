package battlenet

import (
	"encoding/json"

	"gitlab.com/raidkeeper/api/battlenet/schemas/wow"
)

func (w *Wow) GetCharacterMythicKeystoneProfile() (*wow.CharacterMythicKeystoneProfileResponse, error) {
	w.BuildRequest("profile", "profile/wow/character/%s/%s/mythic-keystone-profile", w.Realm, w.CharacterName)

	resp, err := w.Client.Get()
	if err != nil {
		return &wow.CharacterMythicKeystoneProfileResponse{}, err
	}

	o := wow.CharacterMythicKeystoneProfileResponse{}
	err = json.Unmarshal(resp, &o)
	return &o, err
}
