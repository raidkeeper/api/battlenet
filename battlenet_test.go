package battlenet

import (
	"os"
	"testing"
	"time"
)

func CreateBattlenetClient() (*Battlenet, error) {
	return New(&Config{
		ClientId:     os.Getenv("TEST_BATTLENET_CLIENT_ID"),
		ClientSecret: os.Getenv("TEST_BATTLENET_CLIENT_SECRET"),
		Locale:       "en_US",
		Region:       "us",
	})
}

func TestWowTokenRenew(t *testing.T) {
	b, err := CreateBattlenetClient()
	if err != nil {
		t.Error(err)
	}

	if b.Token.Expiration.Unix() <= time.Now().Unix() {
		t.Errorf("token expiration is %d, expected greater than %d", b.Token.Expiration.Unix(), time.Now().Unix())
	}

	if len(b.Token.AccessToken) == 0 {
		t.Error("zero byte access token found, expected an access token with value")
	}
}
