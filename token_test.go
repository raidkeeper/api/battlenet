package battlenet

import (
	"os"
	"testing"
	"time"
)

func TestTokenRenew(t *testing.T) {
	token := &Token{
		ClientId:     os.Getenv("TEST_BATTLENET_CLIENT_ID"),
		ClientSecret: os.Getenv("TEST_BATTLENET_CLIENT_SECRET"),
		Region:       "us",
	}

	err := token.Renew()
	if err != nil {
		t.Error(err)
	}

	if token.Expiration.Unix() <= time.Now().Unix() {
		t.Errorf("token expiration is %d, expected greater than %d", token.Expiration.Unix(), time.Now().Unix())
	}

	if len(token.AccessToken) == 0 {
		t.Error("zero byte access token found, expected an access token with value")
	}
}
