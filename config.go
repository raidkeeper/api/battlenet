package battlenet

type Config struct {
	ClientId     string
	ClientSecret string
	Locale       string
	Region       string
}
