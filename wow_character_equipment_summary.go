package battlenet

import (
	"encoding/json"

	"gitlab.com/raidkeeper/api/battlenet/schemas/wow"
)

func (w *Wow) GetCharacterEquipmentSummary() (*wow.CharacterEquipmentSummaryResponse, error) {
	w.BuildRequest("profile", "profile/wow/character/%s/%s/equipment", w.Realm, w.CharacterName)

	resp, err := w.Client.Get()
	if err != nil {
		return &wow.CharacterEquipmentSummaryResponse{}, err
	}

	o := wow.CharacterEquipmentSummaryResponse{}
	err = json.Unmarshal(resp, &o)
	return &o, err
}
