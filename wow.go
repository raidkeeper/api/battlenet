package battlenet

import (
	"fmt"
	"strings"

	"gitlab.com/raidkeeper/api/battlenet/http"
)

type Wow struct {
	CharacterId   int
	CharacterName string
	Client        *http.Client
	GuildName     string
	GuildId       int
	Locale        string
	Realm         string
	Region        string
}

func (w *Wow) SetCharacter(name string) *Wow {
	w.CharacterName = w.Slugify(name)
	return w
}

func (w *Wow) SetGuild(name string) *Wow {
	w.GuildName = w.Slugify(name)
	return w
}

func (w *Wow) SetRealm(name string) *Wow {
	w.Realm = w.Slugify(name)
	return w
}

func (w *Wow) Slugify(text string) string {
	text = strings.ReplaceAll(text, " ", "-")
	text = strings.ReplaceAll(text, "'", "")
	return strings.ToLower(text)
}

func (w *Wow) BuildRequest(namespace string, endpoint string, args ...any) {
	url := fmt.Sprintf("https://%s/%s", regionalBaseUrls[w.Region], fmt.Sprintf(endpoint, args...))
	w.Client.Url = fmt.Sprintf("%s?locale=%s", url, w.Locale)

	w.Client.AddHeader("Battlenet-Namespace", fmt.Sprintf("%s-%s", namespace, w.Region))
}
