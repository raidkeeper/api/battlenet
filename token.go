package battlenet

import (
	"encoding/json"
	"net/url"
	"strings"
	"time"

	"gitlab.com/raidkeeper/api/battlenet/http"
)

type Token struct {
	AccessToken  string `json:"access_token"`
	ClientId     string
	ClientSecret string
	Expiration   time.Time
	Region       string
}

func (t *Token) Get() (string, error) {
	if t.Expiration.Unix() < time.Now().Unix() {
		if err := t.Renew(); err != nil {
			return "", err
		}
	}
	return t.AccessToken, nil
}

func (t *Token) Renew() error {
	c := http.New(&http.Config{
		BasicAuthUser:     url.QueryEscape(t.ClientId),
		BasicAuthPassword: url.QueryEscape(t.ClientSecret),
	})

	c.AddHeader("Content-Type", "application/x-www-form-urlencoded")

	if t.Region == "cn" {
		c.Url = oauthUrls["cn"]
	} else {
		c.Url = oauthUrls["global"]
	}
	c.Url = "https://" + c.Url + "/oauth/token"

	data := url.Values{}
	data.Set("grant_type", "client_credentials")

	resp, err := c.Post(strings.NewReader(data.Encode()))
	if err != nil {
		return err
	}

	if err = json.Unmarshal(resp, t); err != nil {
		return err
	}
	t.Expiration = time.Now().Add(time.Second * defaultTokenLifetime)
	return nil
}
