package wow

type Attribute struct {
	Key  Link   `json:"key"`
	Name string `json:"name"`
	ID   int    `json:"id"`
}

type Link struct {
	Href string `json:"href"`
}

type RGBAColor struct {
	R int     `json:"r"`
	G int     `json:"g"`
	B int     `json:"b"`
	A float32 `json:"a"`
}
