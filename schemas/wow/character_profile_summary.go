package wow

type CharacterProfileSummaryResponse struct {
	Links struct {
		Self Link `json:"self"`
	} `json:"_links"`
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Gender struct {
		Type string `json:"type"`
		Name string `json:"name"`
	} `json:"gender"`
	Faction struct {
		Type string `json:"type"`
		Name string `json:"name"`
	} `json:"faction"`
	Race           Attribute `json:"race"`
	CharacterClass Attribute `json:"character_class"`
	ActiveSpec     Attribute `json:"active_spec"`
	Realm          struct {
		Key  Link   `json:"key"`
		Name string `json:"name"`
		ID   int    `json:"id"`
		Slug string `json:"slug"`
	} `json:"realm"`
	Guild struct {
		Key   Link   `json:"key"`
		Name  string `json:"name"`
		ID    int    `json:"id"`
		Realm struct {
			Key struct {
				Href string `json:"href"`
			} `json:"key"`
			Name string `json:"name"`
			ID   int    `json:"id"`
			Slug string `json:"slug"`
		} `json:"realm"`
		Faction struct {
			Type string `json:"type"`
			Name string `json:"name"`
		} `json:"faction"`
	} `json:"guild"`
	Level                 int   `json:"level"`
	Experience            int   `json:"experience"`
	AchievementPoints     int   `json:"achievement_points"`
	Achievements          Link  `json:"achievements"`
	Titles                Link  `json:"titles"`
	PvpSummary            Link  `json:"pvp_summary"`
	Encounters            Link  `json:"encounters"`
	Media                 Link  `json:"media"`
	LastLoginTimestamp    int64 `json:"last_login_timestamp"`
	AverageItemLevel      int   `json:"average_item_level"`
	EquippedItemLevel     int   `json:"equipped_item_level"`
	Specializations       Link  `json:"specializations"`
	Statistics            Link  `json:"statistics"`
	MythicKeystoneProfile Link  `json:"mythic_keystone_profile"`
	Equipment             Link  `json:"equipment"`
	Appearance            Link  `json:"appearance"`
	Collections           Link  `json:"collections"`
	ActiveTitle           struct {
		Key           Link   `json:"key"`
		Name          string `json:"name"`
		ID            int    `json:"id"`
		DisplayString string `json:"display_string"`
	} `json:"active_title"`
	Reputations            Link `json:"reputations"`
	Quests                 Link `json:"quests"`
	AchievementsStatistics Link `json:"achievements_statistics"`
	Professions            Link `json:"professions"`
	CovenantProgress       struct {
		ChosenCovenant struct {
			Key  Link   `json:"key"`
			Name string `json:"name"`
			ID   int    `json:"id"`
		} `json:"chosen_covenant"`
		RenownLevel int  `json:"renown_level"`
		Soulbinds   Link `json:"soulbinds"`
	} `json:"covenant_progress"`
}
