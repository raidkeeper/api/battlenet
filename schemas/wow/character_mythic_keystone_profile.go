package wow

type CharacterMythicKeystoneProfileResponse struct {
	Links struct {
		Self Link `json:"self"`
	} `json:"_links"`
	CurrentPeriod struct {
		Period struct {
			Key Link `json:"key"`
			ID  int  `json:"id"`
		} `json:"period"`
	} `json:"current_period"`
	Seasons []struct {
		Key Link `json:"key"`
		ID  int  `json:"id"`
	} `json:"seasons"`
	Character struct {
		Key   Link   `json:"key"`
		Name  string `json:"name"`
		ID    int    `json:"id"`
		Realm struct {
			Key  Link   `json:"key"`
			Name string `json:"name"`
			ID   int    `json:"id"`
			Slug string `json:"slug"`
		} `json:"realm"`
	} `json:"character"`
	CurrentMythicRating struct {
		Color  RGBAColor `json:"color"`
		Rating float64   `json:"rating"`
	} `json:"current_mythic_rating"`
}
