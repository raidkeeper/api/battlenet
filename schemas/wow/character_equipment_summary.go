package wow

type CharacterEquipmentSummaryResponse struct {
	Links struct {
		Self Link `json:"self"`
	} `json:"_links"`
	Character struct {
		Key   Link   `json:"key"`
		Name  string `json:"name"`
		ID    int    `json:"id"`
		Realm struct {
			Key  Link   `json:"key"`
			Name string `json:"name"`
			ID   int    `json:"id"`
			Slug string `json:"slug"`
		} `json:"realm"`
	} `json:"character"`
	EquippedItems []struct {
		Item struct {
			Key Link `json:"key"`
			ID  int  `json:"id"`
		} `json:"item"`
		Slot struct {
			Type string `json:"type"`
			Name string `json:"name"`
		} `json:"slot"`
		Quantity  int   `json:"quantity"`
		Context   int   `json:"context"`
		BonusList []int `json:"bonus_list"`
		Quality   struct {
			Type string `json:"type"`
			Name string `json:"name"`
		} `json:"quality"`
		Name  string `json:"name"`
		Media struct {
			Key Link `json:"key"`
			ID  int  `json:"id"`
		} `json:"media"`
		ItemClass     Attribute `json:"item_class"`
		ItemSubclass  Attribute `json:"item_subclass"`
		InventoryType struct {
			Type string `json:"type"`
			Name string `json:"name"`
		} `json:"inventory_type"`
		Binding struct {
			Type string `json:"type"`
			Name string `json:"name"`
		} `json:"binding"`
		LimitCategory string `json:"limit_category,omitempty"`
		Armor         struct {
			Value   int `json:"value"`
			Display struct {
				DisplayString string    `json:"display_string"`
				Color         RGBAColor `json:"color"`
			} `json:"display"`
		} `json:"armor,omitempty"`
		Stats []struct {
			Type struct {
				Type string `json:"type"`
				Name string `json:"name"`
			} `json:"type"`
			Value   int `json:"value"`
			Display struct {
				DisplayString string    `json:"display_string"`
				Color         RGBAColor `json:"color"`
			} `json:"display"`
			IsNegated    bool `json:"is_negated,omitempty"`
			IsEquipBonus bool `json:"is_equip_bonus,omitempty"`
		} `json:"stats"`
		Spells []struct {
			Spell       Attribute `json:"spell"`
			Description string    `json:"description"`
		} `json:"spells,omitempty"`
		SellPrice struct {
			Value          int `json:"value"`
			DisplayStrings struct {
				Header string `json:"header"`
				Gold   string `json:"gold"`
				Silver string `json:"silver"`
				Copper string `json:"copper"`
			} `json:"display_strings"`
		} `json:"sell_price"`
		Level struct {
			Value         int    `json:"value"`
			DisplayString string `json:"display_string"`
		} `json:"level"`
		Transmog struct {
			Item                     Attribute `json:"item"`
			DisplayString            string    `json:"display_string"`
			ItemModifiedAppearanceID int       `json:"item_modified_appearance_id"`
		} `json:"transmog,omitempty"`
		Durability struct {
			Value         int    `json:"value"`
			DisplayString string `json:"display_string"`
		} `json:"durability,omitempty"`
		Sockets []struct {
			SocketType struct {
				Type string `json:"type"`
				Name string `json:"name"`
			} `json:"socket_type"`
			Item          Attribute `json:"item"`
			DisplayString string    `json:"display_string"`
			Media         struct {
				Key Link `json:"key"`
				ID  int  `json:"id"`
			} `json:"media"`
		} `json:"sockets,omitempty"`
		IsSubclassHidden     bool `json:"is_subclass_hidden,omitempty"`
		ModifiedCraftingStat []struct {
			ID   int    `json:"id"`
			Type string `json:"type"`
			Name string `json:"name"`
		} `json:"modified_crafting_stat,omitempty"`
		Set struct {
			ItemSet Attribute `json:"item_set"`
			Items   []struct {
				Item       Attribute `json:"item"`
				IsEquipped bool      `json:"is_equipped,omitempty"`
			} `json:"items"`
			Effects []struct {
				DisplayString string `json:"display_string"`
				RequiredCount int    `json:"required_count"`
				IsActive      bool   `json:"is_active,omitempty"`
			} `json:"effects"`
			DisplayString string `json:"display_string"`
		} `json:"set,omitempty"`
		NameDescription struct {
			DisplayString string    `json:"display_string"`
			Color         RGBAColor `json:"color"`
		} `json:"name_description,omitempty"`
		Enchantments []struct {
			DisplayString   string `json:"display_string"`
			EnchantmentID   int    `json:"enchantment_id"`
			EnchantmentSlot struct {
				ID   int    `json:"id"`
				Type string `json:"type"`
			} `json:"enchantment_slot"`
		} `json:"enchantments,omitempty"`
		ModifiedAppearanceID int `json:"modified_appearance_id,omitempty"`
		Requirements         struct {
			Level struct {
				Value         int    `json:"value"`
				DisplayString string `json:"display_string"`
			} `json:"level"`
			PlayableClasses struct {
				Links         []Attribute `json:"links"`
				DisplayString string      `json:"display_string"`
			} `json:"playable_classes"`
		} `json:"requirements,omitempty"`
		UniqueEquipped string `json:"unique_equipped,omitempty"`
		Description    string `json:"description,omitempty"`
		Weapon         struct {
			Damage struct {
				MinValue      int    `json:"min_value"`
				MaxValue      int    `json:"max_value"`
				DisplayString string `json:"display_string"`
				DamageClass   struct {
					Type string `json:"type"`
					Name string `json:"name"`
				} `json:"damage_class"`
			} `json:"damage"`
			AttackSpeed struct {
				Value         int    `json:"value"`
				DisplayString string `json:"display_string"`
			} `json:"attack_speed"`
			Dps struct {
				Value         float64 `json:"value"`
				DisplayString string  `json:"display_string"`
			} `json:"dps"`
		} `json:"weapon,omitempty"`
	} `json:"equipped_items"`
	EquippedItemSets []struct {
		ItemSet Attribute `json:"item_set"`
		Items   []struct {
			Item       Attribute `json:"item"`
			IsEquipped bool      `json:"is_equipped,omitempty"`
		} `json:"items"`
		Effects []struct {
			DisplayString string `json:"display_string"`
			RequiredCount int    `json:"required_count"`
			IsActive      bool   `json:"is_active,omitempty"`
		} `json:"effects"`
		DisplayString string `json:"display_string"`
	} `json:"equipped_item_sets"`
}
