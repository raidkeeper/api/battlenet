package battlenet

import (
	"testing"
)

func TestWowCharacterEquipmentSummary(t *testing.T) {
	b, err := CreateBattlenetClient()
	if err != nil {
		t.Error(err)
	}

	w := b.Wow().SetCharacter("Ashkrall").SetRealm("Sargeras")
	resp, err := w.GetCharacterEquipmentSummary()
	if err != nil {
		t.Error(err)
	}

	if resp.Character.Name != "Ashkrall" {
		t.Errorf("expected character name Ashkrall, got %s", resp.Character.Name)
	}

	if len(resp.EquippedItems) == 0 {
		t.Error("expected equipped items, found 0")
	}
}
