package http

import "time"

var (
	defaultTimeout time.Duration = 10 * time.Second
)

type Config struct {
	BasicAuthUser     string
	BasicAuthPassword string
	Timeout           time.Duration
	Insecure          bool
}

func (c *Config) Defaults() {
	if c.Timeout == 0 {
		c.Timeout = defaultTimeout
	}
}
