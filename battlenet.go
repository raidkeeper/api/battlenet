package battlenet

import (
	"fmt"
	"time"

	"gitlab.com/raidkeeper/api/battlenet/http"
)

var (
	defaultTokenLifetime time.Duration = 86399

	oauthUrls = map[string]string{
		"global": "oauth.battle.net",
		"cn":     "oauth.battlenet.com.cn",
	}

	regionalBaseUrls = map[string]string{
		"us": "us.api.blizzard.com",
		"eu": "eu.api.blizzard.com",
		"kr": "kr.api.blizzard.com",
		"tw": "tw.api.blizzard.com",
		"cn": "gateway.battlenet.com.cn",
	}
)

type Battlenet struct {
	Config *Config
	Client *http.Client
	Token  *Token
}

func New(config *Config) (*Battlenet, error) {
	b := &Battlenet{
		Config: config,
	}

	b.Token = &Token{
		ClientId:     b.Config.ClientId,
		ClientSecret: b.Config.ClientSecret,
		Region:       b.Config.Region,
	}
	accessToken, err := b.Token.Get()
	if err != nil {
		return &Battlenet{}, err
	}

	b.Client = http.New(&http.Config{})
	b.Client.AddHeader("Authorization", fmt.Sprintf("Bearer %s", accessToken))
	return b, nil
}

func (b *Battlenet) Wow() *Wow {
	w := &Wow{
		Client: b.Client,
		Locale: b.Config.Locale,
		Region: b.Config.Region,
	}

	return w
}
