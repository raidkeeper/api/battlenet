package battlenet

import (
	"encoding/json"

	"gitlab.com/raidkeeper/api/battlenet/schemas/wow"
)

func (w *Wow) GetCharacterProfileSummary() (*wow.CharacterProfileSummaryResponse, error) {
	w.BuildRequest("profile", "profile/wow/character/%s/%s", w.Realm, w.CharacterName)

	resp, err := w.Client.Get()
	if err != nil {
		return &wow.CharacterProfileSummaryResponse{}, err
	}

	o := wow.CharacterProfileSummaryResponse{}
	err = json.Unmarshal(resp, &o)
	return &o, err
}
