package battlenet

import (
	"testing"
)

func TestWowCharacterProfileSummary(t *testing.T) {
	b, err := CreateBattlenetClient()
	if err != nil {
		t.Error(err)
	}

	w := b.Wow().SetCharacter("Ashkrall").SetRealm("Sargeras")
	resp, err := w.GetCharacterProfileSummary()
	if err != nil {
		t.Error(err)
	}

	if resp.Name != "Ashkrall" {
		t.Errorf("expected character name Ashkrall, got %s", resp.Name)
	}

	if resp.CharacterClass.Name != "Druid" {
		t.Errorf("expected character class Druid, got %s", resp.CharacterClass.Name)
	}
}
