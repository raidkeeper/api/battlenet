package battlenet

import (
	"testing"
)

func TestWowCharacterMythicKeystoneProfile(t *testing.T) {
	b, err := CreateBattlenetClient()
	if err != nil {
		t.Error(err)
	}

	w := b.Wow().SetCharacter("Ashkrall").SetRealm("Sargeras")
	resp, err := w.GetCharacterMythicKeystoneProfile()
	if err != nil {
		t.Error(err)
	}

	if resp.Character.Name != "Ashkrall" {
		t.Errorf("expected character name Ashkrall, got %s", resp.Character.Name)
	}

	if resp.CurrentMythicRating.Rating == 0 {
		t.Error("expected a valid mythic rating, found 0")
	}
}
